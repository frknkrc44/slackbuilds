#!/bin/sh

install -dZ ${DESTDIR}/etc/ly
install -DZ bin/ly -t ${DESTDIR}/usr/bin
install -DZ res/xsetup.sh -t ${DESTDIR}/etc/ly
install -DZ res/wsetup.sh -t ${DESTDIR}/etc/ly
install -DZ res/config.ini -t ${DESTDIR}/etc/ly
install -dZ ${DESTDIR}/etc/ly/lang
install -DZ res/lang/* -t ${DESTDIR}/etc/ly/lang

# Slackware related parts
mv ${DESTDIR}/usr/bin/ly ${DESTDIR}/usr/bin/ly-bin
cat << "EOF" > ${DESTDIR}/usr/bin/ly
#!/bin/sh
. /etc/profile.d/lang.sh
export LANG
exec /usr/bin/ly-bin $*
EOF
chmod +x ${DESTDIR}/usr/bin/ly