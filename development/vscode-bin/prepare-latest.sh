if [ $(uname -m) != "x86_64" ]
then
	echo "It's not x86_64"
	exit 1
fi

LINK="https://code.visualstudio.com/sha/download?build=stable&os=linux-x64"
rm -f code-stable-x64*.tar.gz
wget --content-disposition $LINK

# code-stable-x64-1630494605.tar.gz
REDIRECTED_LINK=$(curl $LINK -s -L -I -o /dev/null -w '%{url_effective}')
FILENAME=$(basename $REDIRECTED_LINK)
SUBVERSION=$(echo $FILENAME | cut -f4 -d'-' | cut -f1 -d'.')
MD5SUM=$(md5sum $FILENAME | cut -f1 -d' ')
sed -e 's ^VERSION=.*$ VERSION="'$SUBVERSION'" ' \
	-e 's ^DOWNLOAD_x86_64=.*$ DOWNLOAD_x86_64="'$REDIRECTED_LINK'" ' \
	-e 's ^MD5SUM_x86_64=.*$ MD5SUM_x86_64="'$MD5SUM'" ' \
	vscode-bin.info > vscode-bin.info.new
mv vscode-bin.info vscode-bin.info.old
mv vscode-bin.info.new vscode-bin.info

sed -e 's ^SRCVER=.*$ SRCVER='$SUBVERSION' ' \
	-e 's ^VERSION=.*$ VERSION="'$SUBVERSION'" ' \
	vscode-bin.SlackBuild > vscode-bin.SlackBuild.new
mv vscode-bin.SlackBuild vscode-bin.SlackBuild.old
mv vscode-bin.SlackBuild.new vscode-bin.SlackBuild
chmod +x vscode-bin.SlackBuild
